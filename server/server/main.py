import os
from dataclasses import dataclass

from sanic import Request, Sanic, response
from serde.json import from_json, to_json

from core import End, GameSettings, GameState, PlayerId, json, Action
from initial_state import initial_state
from perform_action import perform_action


app = Sanic("walk-in-the-park")


@json
@dataclass
class InfoReq:
    settings: GameSettings


@json
@dataclass
class InfoRes:
    player_counts: list[int]


@app.post("/info")
async def info_handler(http_req: Request):
    req = from_json(InfoReq, http_req.body.decode("utf-8"))

    player_counts = req.settings.validated_player_counts()

    res = InfoRes(player_counts=player_counts)
    return response.raw(
        body=to_json(res),
        content_type="application/json",
    )


@json
@dataclass
class InitialStateReq:
    players: list[PlayerId]
    settings: GameSettings
    seed: int


@json
@dataclass
class InitialStateRes:
    state: GameState


@app.post("/initial-state")
async def initial_state_handler(http_req: Request):

    req = from_json(InitialStateReq, http_req.body.decode("utf-8"))

    game_state = initial_state(
        player_ids=req.players, settings=req.settings, seed=req.seed
    )

    if not game_state:
        return response.text("invalid settings and/or player count", status=422)

    res = InitialStateRes(state=game_state)

    return response.raw(body=to_json(res), content_type="application/json")


@json
@dataclass
class PerformActionReq:
    action: Action
    performed_by: PlayerId
    state: GameState
    settings: GameSettings
    seed: int


@json
@dataclass
class PerformActionRes:
    next_state: GameState
    completed: bool


@app.post("/perform-action")
async def perform_action_handler(http_req: Request):

    req = from_json(PerformActionReq, http_req.body.decode("utf-8"))

    state = perform_action(
        action=req.action,
        performed_by=req.performed_by,
        previous_state=req.state,
        settings=req.settings,
        seed=req.seed,
    )

    if not state:
        return response.empty(status=422)

    res = PerformActionRes(next_state=state, completed=isinstance(state.phase, End))

    return response.raw(body=to_json(res), content_type="application/json")


if __name__ == "__main__":

    port = int(os.environ["PORT"])
    debug = bool(os.environ.get("DEBUG", False))

    app.run(host="0.0.0.0", port=port, fast=not debug, debug=debug)
