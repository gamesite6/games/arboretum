from copy import deepcopy
from random import Random
from typing import Optional
from core import *


def is_action_allowed(
    action: Action,
    performed_by: PlayerId,
    state: GameState,
    settings: GameSettings,
) -> bool:

    actor = next(player for player in state.players if player.id == performed_by)

    match (state.phase, action):
        case (Drawing(player), Draw(None)):
            return player == actor.id and len(state.deck) > 0
        case (Drawing(player), Draw(from_player=from_player)):
            pile: list[CardId] = next(
                (
                    player.discarded
                    for player in state.players
                    if player.id == from_player
                ),
                [],
            )
            return player == actor.id and len(pile) > 0

        case (Playing(player), Play(card=card, position=position)):
            return (
                player == actor.id
                and card in actor.hand
                and not position in actor.played
            )

        case (Discarding(player), Discard(card)):
            return player == actor.id and card in actor.hand

        case (Scoring(ready=ready, paths=paths), Score(suit=suit, path=path)):
            return (
                performed_by not in ready
                and is_player_scoring_suit(state.players, suit, performed_by)
                and is_valid_path(actor.played, suit, path)
            )

        case (Scoring(ready=ready), Ready()):
            return performed_by not in ready

        case (Scoring(ready=ready), Unready()):
            return performed_by in ready

        case _:
            return False


def perform_action(
    action: Action,
    performed_by: PlayerId,
    previous_state: GameState,
    settings: GameSettings,
    seed: int,
) -> Optional[GameState]:

    if not is_action_allowed(action, performed_by, previous_state, settings):
        return None

    state = deepcopy(previous_state)
    # rng = Random(seed)

    actor = next(player for player in state.players if player.id == performed_by)
    actor_idx = state.players.index(actor)

    match state.phase:
        case Drawing(_):
            match action:
                case Draw(from_player):
                    target_pile = (
                        state.deck
                        if from_player is None
                        else next(
                            player.discarded
                            for player in state.players
                            if player.id == from_player
                        )
                    )

                    card = target_pile.pop(-1)
                    actor.hand.append(card)
                    state.phase.cards_to_draw -= 1

                    if state.phase.cards_to_draw == 0:
                        state.phase = Playing(player=performed_by)
                    return state

        case Playing(_):
            match action:
                case Play(card, position):
                    actor.hand.remove(card)
                    actor.played[position] = card
                    state.phase = Discarding(player=performed_by)
                    return state

        case Discarding():
            match action:
                case Discard(card):
                    actor.hand.remove(card)
                    actor.discarded.append(card)

                    if len(state.deck) == 0:
                        state.phase = Scoring(ready=set(), paths={})
                        pass
                    else:
                        next_player_idx = (actor_idx + 1) % len(state.players)
                        state.phase = Drawing(
                            player=state.players[next_player_idx].id, cards_to_draw=2
                        )
                    return state
        case Scoring():
            match action:
                case Score(suit=suit, path=path):

                    paths = state.phase.paths.setdefault(str(actor.id), {})
                    paths[suit] = path

                    return state

                case Ready():
                    state.phase.ready.add(actor.id)
                    if all(p.id in state.phase.ready for p in state.players):
                        state.phase = End(paths=state.phase.paths)
                    return state

                case Unready():
                    state.phase.ready.remove(actor.id)
                    return state

    return state
