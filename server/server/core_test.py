from .core import (
    Player,
    card_id,
    card_value,
    card_suit,
    is_player_scoring_suit,
    is_valid_path,
    parse_position,
)


def test_card_values():
    assert card_value(5) == 5
    assert card_value(11) == 3
    assert card_value(16) == 8
    assert card_value(17) == 1
    assert card_value(80) == 8


def test_card_suits():
    assert card_suit(1) == "A"
    assert card_suit(8) == "A"
    assert card_suit(9) == "B"
    assert card_suit(72) == "I"
    assert card_suit(80) == "J"


def test_parse_position():
    assert parse_position("1,1") == (1, 1)
    assert parse_position("-1,3") == (-1, 3)
    assert parse_position("-2,-5") == (-2, -5)
    assert parse_position("0,-12") == (0, -12)


def test_card_id():
    for i in range(1, 81):
        assert card_id(card_value(i), card_suit(i)) == i


def test_is_player_scoring_suit():
    players = [
        Player(
            id=1,
            hand=[card_id(2, "A"), card_id(4, "A"), card_id(1, "B")],
            played={},
            discarded=[],
        ),
        Player(id=2, hand=[card_id(7, "A"), card_id(3, "C")], played={}, discarded=[]),
        Player(
            id=3,
            hand=[card_id(8, "B"), card_id(1, "C"), card_id(2, "C")],
            played={},
            discarded=[],
        ),
    ]

    # even though player1 has more A's, player2 has the higher value
    assert is_player_scoring_suit(players, "A", 1) == False
    assert is_player_scoring_suit(players, "A", 2) == True

    # player3's 8 is zeroed out by player1's ace.
    assert is_player_scoring_suit(players, "B", 1) == True
    assert is_player_scoring_suit(players, "B", 3) == False

    # player2 and player3 are tied for most C's
    assert is_player_scoring_suit(players, "C", 1) == False
    assert is_player_scoring_suit(players, "C", 2) == True
    assert is_player_scoring_suit(players, "C", 3) == True

    # noone has D's. Everyone can score!
    assert is_player_scoring_suit(players, "D", 1) == True
    assert is_player_scoring_suit(players, "D", 2) == True
    assert is_player_scoring_suit(players, "D", 3) == True


def test_is_valid_path():
    played = {
        # row 1
        "-1,-1": card_id(6, "B"),
        "0,-1": card_id(5, "B"),
        "1,-1": card_id(4, "B"),
        "2,-1": card_id(1, "B"),
        # row 2
        "-1,0": card_id(8, "C"),
        "0,0": card_id(2, "A"),
        "1,0": card_id(3, "C"),
        "2,0": card_id(6, "D"),
        # row 3
        "-1,1": card_id(3, "E"),
        "1,1": card_id(6, "A"),
    }

    assert is_valid_path(played, "A", ["0,0", "1,0", "1,1"]) == True

    # may not move diagonally
    assert is_valid_path(played, "A", ["0,0", "1,1"]) == False

    assert is_valid_path(played, "B", ["2,-1", "1,-1", "0,-1", "-1,-1"]) == True
    assert is_valid_path(played, "C", ["1,0", "1,-1", "0,-1", "-1,-1", "-1,0"]) == True

    # must end on same suit as first card
    assert is_valid_path(played, "C", ["1,0", "1,-1", "0,-1", "-1,-1"]) == False
