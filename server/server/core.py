from dataclasses import dataclass
from typing import Final, Optional, Union

from serde.core import InternalTagging
from serde import serde


def json(_cls):
    return serde(_cls, tagging=InternalTagging("kind"), rename_all="camelcase")


VALID_PLAYER_COUNTS: Final = (2, 3, 4)


PlayerId = int
PlayerIdStr = str
CardId = int
Suit = str


def card_value(card: CardId):
    zero_indexed_card = card - 1
    zero_indexed_value = zero_indexed_card % 8
    return zero_indexed_value + 1


suits: Final = ("A", "B", "C", "D", "E", "F", "G", "H", "I", "J")


def card_suit(card: CardId):
    zero_indexed_card = card - 1
    suit_index = zero_indexed_card // 8
    return suits[suit_index]


def card_id(value: int, card_suit: Suit) -> CardId:
    return suits.index(card_suit) * 8 + value


@json
@dataclass
class GameSettings:
    """Game settings"""

    player_counts: list[int]

    def validated_player_counts(self) -> list[int]:
        return [c for c in VALID_PLAYER_COUNTS if c in self.player_counts]


CardPosition = str


def parse_position(s: CardPosition) -> tuple[int, int]:
    [x, y] = s.split(",")
    return (int(x), int(y))


def are_adjacent(a: tuple[int, int], b: tuple[int, int]) -> bool:
    (ax, ay) = a
    (bx, by) = b

    return abs(ax - bx) + abs(ay - by) == 1


@json
@dataclass
class Player:
    """Player"""

    id: PlayerId
    hand: list[CardId]
    played: dict[CardPosition, CardId]
    discarded: list[CardId]


@json
@dataclass
class Drawing:
    player: PlayerId
    cards_to_draw: int


@json
@dataclass
class Playing:
    player: PlayerId


@json
@dataclass
class Discarding:
    player: PlayerId


@json
@dataclass
class Scoring:
    ready: set[PlayerId]
    paths: dict[PlayerIdStr, dict[Suit, list[CardPosition]]]


@json
@dataclass
class End:
    paths: dict[PlayerIdStr, dict[Suit, list[CardPosition]]]


Phase = Union[Drawing, Playing, Discarding, Scoring, End]


@json
@dataclass
class GameState:
    """Game state"""

    deck: list[CardId]
    players: list[Player]
    phase: Phase


@json
@dataclass
class Draw:
    from_player: Optional[PlayerId]


@json
@dataclass
class Play:
    card: CardId
    position: CardPosition


@json
@dataclass
class Discard:
    card: CardId


@json
@dataclass
class Score:
    suit: Suit
    path: list[CardPosition]


@json
@dataclass
class Ready:
    pass


@json
@dataclass
class Unready:
    pass


Action = Union[Draw, Play, Discard, Score, Ready, Unready]


def is_player_scoring_suit(
    players: list[Player], suit: Suit, player_id: PlayerId
) -> bool:
    ace_id = card_id(1, suit)
    ace_holder: Optional[PlayerId] = next(
        (p.id for p in players if ace_id in p.hand), None
    )

    player_total = 0
    max_total = 0

    for p in players:
        total = 0
        for c in (c for c in p.hand if card_suit(c) == suit):
            value = card_value(c)
            if value == 8 and ace_holder is not None and ace_holder is not p.id:
                value = 0
            total += value

        max_total = max(max_total, total)
        if p.id == player_id:
            player_total = total

    return player_total >= max_total


def is_valid_path(
    played: dict[CardPosition, CardId], suit: Suit, path: list[CardPosition]
) -> bool:
    if len(path) < 2:
        return False

    previous_pos: Optional[tuple[int, int]] = None
    previous_value = 0

    for pos in path:
        card = played[pos]
        if card is None:
            return False

        value = card_value(card)
        if value <= previous_value:
            return False

        parsed_pos = parse_position(pos)
        if previous_pos is not None and not are_adjacent(parsed_pos, previous_pos):
            return False

        previous_pos = parsed_pos
        previous_value = value

    first_card = played[path[0]]
    last_card = played[path[-1]]

    return card_suit(first_card) is suit and card_suit(last_card) is suit
