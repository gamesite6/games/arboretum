from random import Random
from typing import Optional
from core import CardId, Drawing, GameSettings, GameState, Player, PlayerId


def initial_state(
    player_ids: list[PlayerId], settings: GameSettings, seed: int
) -> Optional[GameState]:
    player_count = len(player_ids)

    if len(set(player_ids)) != player_count:
        return None

    if player_count not in settings.validated_player_counts():
        return None

    rng = Random(seed)

    deck = create_deck(player_count)
    rng.shuffle(deck)

    players = []
    for player_id in player_ids:
        hand = deck[-7:]
        del deck[-7:]
        players.append(Player(id=player_id, hand=hand, played={}, discarded=[]))

    starting_player = rng.choice(player_ids)

    state = GameState(
        deck=deck,
        players=players,
        phase=Drawing(player=starting_player, cards_to_draw=2),
    )

    return state


def create_deck(player_count: int) -> list[CardId]:
    match player_count:
        case 2:
            return list(range(1, 49))
        case 3:
            return list(range(1, 65))
        case _:
            return list(range(1, 81))
