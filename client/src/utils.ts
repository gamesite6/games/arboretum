import { createEventDispatcher } from "svelte";

export function createActionDispatcher(): (action: Action) => void {
  const dispatch = createEventDispatcher();
  return (action: Action) => {
    dispatch("action", action);
  };
}

export function isPlayerActive(state: GameState, playerId: PlayerId): boolean {
  const { phase } = state;

  switch (phase.kind) {
    case "Drawing":
      return phase.player === playerId;
    case "Playing":
      return phase.player === playerId;
    case "Discarding":
      return phase.player === playerId;
    case "Scoring":
      return !phase.ready.includes(playerId);
  }
}

const suits: Suit[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];

function getActiveSuits(playerCount: number) {
  switch (playerCount) {
    case 2:
      return suits.slice(0, 6);
    case 3:
      return suits.slice(0, 8);
    default:
      return suits;
  }
}

export function getCardSuit(card: CardId): Suit {
  const zeroIndexedCard = card - 1;
  const suitIndex = (zeroIndexedCard / 8) | 0;
  return suits[suitIndex];
}

export function getSuitEmoji(suit: Suit): string {
  switch (suit) {
    case "A":
      return "🌸";
    case "B":
      return "🌻";
    case "C":
      return "🌳";
    case "D":
      return "🐿️";
    case "E":
      return "🐦";
    case "F":
      return "🦢";
    case "G":
      return "🪲";
    case "H":
      return "🍁";
    case "I":
      return "🌲";
    case "J":
      return "🍄";
  }
}

export function getCardValue(card: CardId): CardValue {
  const zeroIndexedCard = card - 1;
  const zeroIndexedValue = zeroIndexedCard % 8;
  return (zeroIndexedValue + 1) as CardValue;
}

export function parsePosition(positionKey: string): [number, number] {
  return positionKey.split(",").map((str) => parseInt(str)) as [number, number];
}

export function positionToKey(position: [number, number]): string {
  return position.toString();
}

type GridInfo = {
  cols: number;
  rows: number;
  xMin: number;
  yMin: number;
};

export function getGridInfo(positions: CardPosition[]): GridInfo {
  if (!positions.length) {
    return { cols: 1, rows: 1, xMin: 0, yMin: 0 };
  }

  const parsed = positions.map((str) => parsePosition(str));
  const xs = parsed.map((pos) => pos[0]);
  const ys = parsed.map((pos) => pos[1]);

  const xMin = Math.min(...xs);
  const yMin = Math.min(...ys);

  return {
    cols: Math.max(...xs) - xMin + 1 + 2,
    rows: Math.max(...ys) - yMin + 1 + 2,
    xMin: xMin - 1,
    yMin: yMin - 1,
  };
}

export function getPlaceablePositions(
  positions: CardPosition[]
): CardPosition[] {
  const alreadyPlaced = new Set<CardPosition>();
  const placeable = new Set<CardPosition>(["0,0"]);

  for (let pos of positions) {
    alreadyPlaced.add(pos);

    const [x, y] = parsePosition(pos);

    placeable.add(positionToKey([x + 1, y]));
    placeable.add(positionToKey([x - 1, y]));
    placeable.add(positionToKey([x, y + 1]));
    placeable.add(positionToKey([x, y - 1]));
  }

  for (let pos of alreadyPlaced) {
    placeable.delete(pos);
  }

  const result = [...placeable.values()];

  return result;
}

type SuitTotals = { [suit in Suit]?: number };
type SuitPlayers = { [suit in Suit]?: PlayerId };

function getSuitTotals(player: Player, aceHolders: SuitPlayers): SuitTotals {
  const totals = {};

  for (let card of player.hand) {
    const suit = getCardSuit(card);

    const previousTotal = totals[suit] ?? 0;

    let value = getCardValue(card) as number;

    if (value === 8 && aceHolders[suit] && aceHolders[suit] !== player.id) {
      value = 0;
    }

    totals[suit] = previousTotal + value;
  }

  return totals;
}

export function getAceHolders(players: Player[]): {
  [suit in Suit]?: PlayerId;
} {
  const aceHolders = {};

  for (const player of players) {
    const ace: CardId | undefined = player.hand.find(
      (cardId) => getCardValue(cardId) === 1
    );

    if (ace !== undefined) {
      const suit = getCardSuit(ace);
      aceHolders[suit] = player.id;
    }
  }

  return aceHolders;
}

export function getSuitScorers(players: Player[]): {
  bySuit: { [suit in Suit]?: { maxCount: number; scorers: PlayerId[] } };
  byPlayer: { [playerId: number]: { suits: Suit[] } };
} {
  const playerCount = players.length;
  const suits = getActiveSuits(playerCount);

  const bySuit: { [suit in Suit]?: { maxCount: number; scorers: PlayerId[] } } =
    {};

  const aceHolders = getAceHolders(players);
  const playerSuitCounts: [PlayerId, SuitTotals][] = players.map((p) => [
    p.id,
    getSuitTotals(p, aceHolders),
  ]);

  for (let suit of suits) {
    let maxCount = 0;
    let players: PlayerId[] = [];

    for (let [player, suitCounts] of playerSuitCounts) {
      const count = suitCounts[suit] ?? 0;
      if (count === maxCount) {
        players.push(player);
      } else if (count > maxCount) {
        maxCount = count;
        players = [player];
      }
    }

    bySuit[suit] = { maxCount, scorers: players };
  }

  const byPlayer: { [playerId: number]: { suits: Suit[] } } = {};

  for (let player of players) {
    byPlayer[player.id] = { suits: [] };
  }

  for (let [suit, { scorers }] of Object.entries(bySuit)) {
    for (let scorer of scorers) {
      byPlayer[scorer].suits.push(suit as Suit);
    }
  }

  return { bySuit, byPlayer };
}

export function areAdjacent(a: CardPosition, b: CardPosition): boolean {
  const [aX, aY] = parsePosition(a);
  const [bX, bY] = parsePosition(b);

  return Math.abs(aX - bX) + Math.abs(aY - bY) === 1;
}

export function scorePath(
  played: { [pos: CardPosition]: CardId },
  path: CardPosition[]
): number | undefined {
  if (path.length < 2) {
    return;
  }

  const cards = path.map((pos) => played[pos]);

  if (cards.some((card) => card === undefined)) {
    return;
  }

  const sameSuit = cards.every(
    (card) => getCardSuit(card) === getCardSuit(cards[0])
  );

  let score = cards.length >= 4 && sameSuit ? cards.length * 2 : cards.length;

  if (getCardValue(cards[0]) === 1) {
    score += 1;
  }

  if (getCardValue(cards[cards.length - 1]) === 8) {
    score += 2;
  }

  return score;
}

export function isValidPath(
  played: { [pos: CardPosition]: CardId },
  suit: Suit,
  path: CardPosition[]
): boolean {
  if (path.length < 2) {
    return false;
  }

  const firstPos = path[0];
  const lastPos = path[path.length - 1];

  const firstCard = played[firstPos];
  const lastCard = played[lastPos];

  if (
    !firstCard ||
    !lastCard ||
    getCardSuit(firstCard) !== suit ||
    getCardSuit(lastCard) !== suit
  ) {
    return false;
  }

  let previousPos = undefined;
  let previousValue = 0;

  /* check that cards are of ascending values and adjacent positions */
  for (let pos of path) {
    const card = played[pos];

    if (!card) {
      return false;
    }
    const value = getCardValue(card);
    if (value <= previousValue) {
      return false;
    }

    previousValue = value;

    if (previousPos && !areAdjacent(previousPos, pos)) {
      return false;
    }
    previousPos = pos;
  }

  return true;
}

export function isValidNextStep(
  played: { [pos: CardPosition]: CardId },
  suit: Suit,
  path: CardPosition[],
  pos: CardPosition
): boolean {
  const card = played[pos];
  if (card === undefined) {
    return false;
  }

  if (path.length === 0) {
    return getCardSuit(card) === suit;
  }

  const previousPos = path[path.length - 1];
  const previousCard = played[previousPos];
  if (previousCard === undefined) {
    return false;
  }

  return (
    areAdjacent(previousPos, pos) &&
    getCardValue(previousCard) < getCardValue(card)
  );
}

export type ScoredPlayer = {
  id: PlayerId;
  score: number;
  parkSuits: number;
  rankValue: number;
};

export function scorePlayer(
  player: Player,
  paths: SuitPaths = {}
): ScoredPlayer {
  let score = 0;
  let suits = new Set(Object.values(player.played).map((c) => getCardSuit(c)));
  const parkSuits = suits.size;

  for (let [_, path] of Object.entries(paths)) {
    score += scorePath(player.played, path);
  }

  return {
    id: player.id,
    score,
    parkSuits,
    rankValue: score * 100 + parkSuits,
  };
}
