import { parsePosition, positionToKey, areAdjacent } from "./utils";
import { test, expect } from "vitest";

test("parse position", () => {
  const [x, y] = parsePosition("-1,5");
  expect(x).toBe(-1);
  expect(y).toBe(5);
});

test("position to key", () => {
  const key = positionToKey([3, 4]);
  expect(key).toBe("3,4");
});

test("adjacent", () => {
  expect(areAdjacent("0,0", "1,0")).toBe(true);
  expect(areAdjacent("1,-1", "1,0")).toBe(true);
  expect(areAdjacent("0,0", "1,1")).toBe(false);
  expect(areAdjacent("5,4", "5,2")).toBe(false);
});
