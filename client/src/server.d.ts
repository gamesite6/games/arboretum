type GameSettings = { playerCounts: number[] };
type GameState = {
  deck: CardId[];
  players: Player[];
  phase: Phase;
};

type Phase =
  | { kind: "Drawing"; player: PlayerId; cardsToDraw: number }
  | { kind: "Playing"; player: PlayerId }
  | { kind: "Discarding"; player: PlayerId }
  | { kind: "Scoring"; ready: PlayerId[]; paths: PlayerSuitPaths }
  | { kind: "End"; paths: PlayerSuitPaths };

type PlayerId = number;
type CardId = number;

type PlayerSuitPaths = { [playerId: number]: SuitPaths };
type SuitPaths = { [suit in Suit]?: Path };

type CardPosition = string;
type Path = CardPosition[];

type Player = {
  id: PlayerId;
  hand: CardId[];
  played: { [cardPos: CardPosition]: CardId };
  discarded: CardId[];
};

type Action =
  | { kind: "Draw"; fromPlayer?: PlayerId }
  | { kind: "Play"; card: CardId; position: CardPosition }
  | { kind: "Discard"; card: CardId }
  | { kind: "Score"; suit: Suit; path: Path }
  | { kind: "Ready" }
  | { kind: "Unready" };

type Suit = "A" | "B" | "C" | "D" | "E" | "F" | "G" | "H" | "I" | "J";
type CardValue = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8;
