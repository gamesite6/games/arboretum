export function getDefaultSettings() {
  return {
    playerCounts: [2, 3, 4],
  };
}

export const validPlayerCounts = [2, 3, 4];

export function playerCounts(settings: GameSettings): number[] {
  return validPlayerCounts.filter((c) => settings.playerCounts.includes(c));
}
