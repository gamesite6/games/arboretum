import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    emptyOutDir: false,
    lib: {
      entry: "src/index.ts",
      fileName: "walk-in-the-park",
      name: "Gamesite6_WalkInThePark",
    },
  },
  plugins: [svelte()],
});
